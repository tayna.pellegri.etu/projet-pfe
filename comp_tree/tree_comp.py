from Bio import Phylo





taxo_keys=[]
taxo_dico = dict()

def taxonomy(trees,taxonow=[]):
    """
    Build a dictionnary with bebeast for keys, and value their taxonomy
    
    """
    global taxo_dico
    try:
        node = str(trees.clade)
        for glagla in trees.clade: # parcours pour chaque sous-arbre/clade du niveau actuel
            glagla = Phylo.Newick.Tree(glagla) # conversion de types de clade à tree
            
            # Si le noeud n'est pas terminal
            if glagla.is_terminal() == False: # Si le sous-arbre actuel ne contient que des feuilles 
                # On stocke le nom du noeud pour la taxo
                # Pour chaque sous-arbres,
                taxonomy(glagla,taxonow+[node])# On rappelle la fonction avec la taxo actuelle, i.e. taxo précédente + nom du noeud
                # Une fois fini, cela signifie que le noeud actuel n'est pas utile, donc à voir s'il faut supprimer quelque chose xd
                        
            else:
                # Sinon si le noeud est terminal
                leaf = str(glagla.clade) # On stocke le nom du noeud pour la taxo
                print(leaf)
                taxo_dico[leaf] = taxonow # On rajoute dans le dico, ce nom comme clé, avec comme valeur, la taxo accumulée.
    
    except:
        pass

                
                
 
    
        

def big_intersection(list_of_set):
    """
        Return a sequence of intersection between sets
        
    """
    if len(list_of_set) == 2:
        return list_of_set[0].intersection(list_of_set[1])
    else:
        return list_of_set[-1].intersection(big_intersection(list_of_set[:-1]))


def phylo_coco(trees,taxo_dico,depth=0, threshold = 0):
    """

    Pour comparer un arbre et la taxonomie associée
    
    """
    if depth==0:
        print("=========\n||START||\n=========")
    #print("nb de sous arbre : " + str(len(trees.clade)))
    try:
        for glagla in trees.clade: # parcours pour chaque sous-arbre du niveau actuel
            path = list() # l'ensemble des chemins succéssifs.
            glagla = Phylo.Newick.Tree(glagla) # conversion de types sinon l'attribut clade des Tree devient clades pour les clades
            leaves = glagla.get_terminals() # la liste de feuilles terminales

            for leaf in leaves: # pour chaque feuille du sous arbre
                for key in taxo_dico.keys(): # pour chaque espèce, on récupère la taxo associée
                    if key in leaf.name:
                        path.append(taxo_dico[key])
            path = [ set(p) for p in path  ] # on transforme la liste de liste, en liste d'ensemble
            #print(path)
            #print(big_intersection(path))
            if depth < len(path[0]) - threshold and len(big_intersection(path))!= depth +2: # Si tout va PAS bien, c'est à dire qu'il y a moins de points communs entre les feuilles qu'il y a de chemins depuis la racine.
                print("///////////////////////")
                print("Problème dans l'arbre suivant :")
                print(glagla)
                for espece in glagla.get_terminals():
                    print(espece.name)
                print("profondeur : "+str(depth))
                
        for glagla in trees.clade: # parcours pour chaque sous-arbre du niveau actuel
            if glagla.is_terminal() == False:
                glagla = Phylo.Newick.Tree(glagla)
                #Phylo.draw_ascii(glagla)
                phylo_coco(glagla,taxo_dico,depth+1,threshold)

    except:
        pass
    if depth==0:
        print("=========\n|| END ||\n=========")

#Pour chaque sous-arbre
    # pour chaque espèce
        # on conserve le chemin de l'espèce depuis la racine
    # On regarde l'intersection des chemins
    # Si la longueur de l'intersection est égale à la profondeur
        # tout va bien
    # Sinon
        # on renvoit/affiche l'endroit où ça va pas

    




if __name__=='__main__':
    tree = Phylo.read("newick1.txt","newick")
    #print(tree) # affiche une structure peu lisible
    #Phylo.draw_ascii(tree) # affichage bien plus agréable
    taxo = Phylo.read("taxo.txt","newick")
    Phylo.draw_ascii(taxo)
    taxonomy(taxo)
    phylo_coco(tree,taxo_dico, threshold=0)
