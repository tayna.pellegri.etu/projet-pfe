from Bio import Phylo
import sys

def all_parents(tree):
    """Return a dictionary mapping all nodes in the tree to their parent node .

    Args:
        tree (Tree): a phylogenic tree

    Returns:
        dict: dictionnary mapping all nodes to their parent node
    """
    parents = {}
    for clade in tree.find_clades(order="level"):
        for child in clade:
            parents[child] = clade
    return parents


def remove_not_common_clades(tree1, tree2):
    """Remove clades from tree1 not present in tree2 .

    Args:
        tree1 (Tree): tree with the species needed to be removed
        tree2 (Tree): the tree we're doing the comparison with
    """
    for clade in tree1.get_terminals():
        if not tree2.find_any(clade.name):
            tree1.collapse(clade)


def get_dico_clade_non_terminal(tree_taxo):
    """Returns a dictionary mapping all the non terminal nodes to their child leaves (species) in the taxonomy.

    Args:
        tree_taxo (Tree): taxonomic tree

    Returns:
        dict: dictionary mapping all the non terminal nodes to their child leaves (species) in the taxonomy
    """
    dico_clade_taxo = {}
    intern_nodes = tree_taxo.get_nonterminals()
    intern_nodes += tree_taxo.find_clades("Eutheria")
    for clade in intern_nodes:
        node_childs = clade.clades
        for child in node_childs:
            if not child.is_terminal():
                node_childs += get_leaf_nodes(child)
        dico_clade_taxo[clade.name] = list(dict.fromkeys([x for x in node_childs if x.is_terminal()]))
    return dico_clade_taxo

def get_clade_mrca_phylo(tree_phylo, dico_clade_taxo):
    """Get the mrca (most recent common ancestor) subtree in the phylogenetic tree for all species of a clade (a non terminal node). For all clade in the taxonomy.

    Args:
        tree_phylo (Tree): the phylogenetic tree
        dico_clade_taxo (dict): the dictionary mapping all the non terminal nodes to their child leaves (species) in the taxonomy

    Returns:
        dict: dictionary mapping all the non terminal node (clade) from the taxonomy with the mrca subtree (the mrca of all the species of the clade in the taxonomy) in the phylogenetic tree.
    """
    dico_mrca = {}
    for clade in dico_clade_taxo:
        liste_mrca = []
        mrca = tree_phylo.common_ancestor([x.name for x in dico_clade_taxo[clade]])
        if mrca.clades==[]:
            liste_mrca.append(mrca)
            dico_mrca[clade] = liste_mrca
        else:
            for node in mrca.clades:
                if not node.is_terminal():
                    mrca.clades += get_leaf_nodes(node)
            dico_mrca[clade] = list(dict.fromkeys([x for x in mrca.clades if x.is_terminal()]))
    return dico_mrca

def get_leaf_nodes(node):
    """Get all leaf nodes of a node (clade).

    Args:
        node (Clade): the clade (subtree) we want to have all the leaves from

    Returns:
        list: list containing all the leaves of the clade (subtree)
    """
    leaves = []
    collect_leaf_nodes(node,leaves)
    return leaves

def collect_leaf_nodes(clade, leaves):
    """Collect all the leaves from a clade recursively

    Args:
        clade (Clade): the clade we want to collect the leaves from
        leaves (list): the list containing all the leaves collected from the clade
    """
    if clade is not None:
        if len(clade.clades) == 0:
            leaves.append(clade)
        for n in clade.clades:
            collect_leaf_nodes(n, leaves)


def count_misplaced_clade(dico_clade_taxo, dico_mrca):
    """Counts the misplaced clade in the phylogenetic tree. Compare the species of a clade (non terminal node) in the taxonomy with the mrca subtree of a phylogenetic tree.

    Args:
        dico_clade_taxo (dict): the dictionary mapping all the non terminal nodes to their child leaves (species) in the taxonomy
        dico_mrca (dict): the dictionary mapping all the non terminal nodes (clades) from the taxonomy with the mrca subtree (the mrca of all the species of the clade in the taxonomy) in the phylogenetic tree

    Returns:
        tuple: a tuple containing the number of misplaced clade and the list of clades misplaced
    """
    count_false=0
    clade_misplaced = []
    for clade in dico_clade_taxo:
        liste_taxo = sorted([x.name for x in dico_clade_taxo[clade]])
        liste_phylo = sorted([x.name for x in dico_mrca[clade]])
        if liste_taxo != liste_phylo:
            count_false += 1
            clade_misplaced.append(clade)
            title = "|"+"Pour la clade {} :".format(clade) + "|"
            for i in range(len(title)):
                tiret = "-"*i
            print(tiret)
            print("|"+"Pour la clade {} :".format(clade) + "|")
            print(tiret)
            species_missplaced = set(liste_taxo) ^ set(liste_phylo)
            print("Les espèces mal placées:")
            print(list(species_missplaced))
            print("=========================================")
    if count_false == 0:
        print("All mrca trees match with the clade in the taxonomy")

    pourcentage = ((len(dico_clade_taxo)-count_false)/len(dico_clade_taxo))*100
    print("Pourcentage de clades bien classés : {}%".format(pourcentage))

    return count_false, clade_misplaced

if __name__ == "__main__":
    tree_phylo = Phylo.read(sys.argv[1], "newick")
    tree_phylo.rooted = True
    tree_taxo = Phylo.read(sys.argv[2], "newick")
    tree_taxo.rooted = True

    remove_not_common_clades(tree_taxo, tree_phylo) #on enlève les espèces qui ne sont pas présentes dans la phylogénie

    dico_taxo = get_dico_clade_non_terminal(tree_taxo)
    
    dico_mrca_phylo = get_clade_mrca_phylo(tree_phylo, dico_taxo)
    
    print(count_misplaced_clade(dico_taxo, dico_mrca_phylo))
