# projet PFE_Comp_tree

Programme permettant de comparer un arbre phylogénétique à une taxonomie et d'en vérifier la cohérence.  

## Utilisation

python comp_tree.py ['fichier_arbre_phylogénétique'] ['fichier_arbre_taxonomique']  

## Fichiers d'entrée

Les fichiers doivent être sous le format Newick.

## Contraintes techniques

- Python 3
- Biopython 1.79 (https://biopython.org/)
